# instalar docker for windows

docker ps
docker version
docker info

docker run hello-world
# https://github.com/docker-library/hello-world/tree/master/hello-world


docker run -it busybox bash
# docker run -it ubuntu:16.04 bash
# docker run -it ubuntu:14.04 bash
# ...
# cat /etc/issue

docker run -d -p 80:80 --name webserver nginx
# visitar localhost
docker ps
docker stop dbc287b2ca76
docker start dbc287b2ca76
docker exec -ti dbc287b2ca76 bash

# dentro del container
cd /usr/share/nginx/html/
# visital localhost/index2.html
echo 'aaaaaaaaaaaaaaa' > index2.html
# visital localhost/index2.html
rm index2.html
cp index.html index2.html
# visital localhost/index2.html

# ejemplo
cd ejemplo/docker/propio/php
docker build -t my_php .
docker run my_php
docker exec -ti 8d35bc87e0f0 bash
vim /var/www/html/info.php

### cd ejemplo/docker/mysql
### docker build -t my_mysql .
### docker run my_mysql
### docker exec -ti fba9a0710535 bash
### mysql -p
### ## dentro de mysql
### create database docker_test;
### use docker_test;
### CREATE TABLE conf (name VARCHAR(30) NOT NULL, value VARCHAR(30) NOT NULL);
### INSERT INTO conf (name,value) VALUES('docker','es lo maximo');
### select * from conf;

# ejemplo compose
cd ejemplo/docker/ajeno
# chequear sintaxis del archivo y mostrar data folder vacio
docker-compose up
# .gitignore data

# hacer lo mismo en el servidor de prueba (con un puerto distinto al 80)
docker-compose stop
docker-compose rm
docker volume ls -qf dangling=true | xargs -r docker volume rm
