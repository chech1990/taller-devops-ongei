# crearse usuario en bitbucket
# instalas git for windows

# Branching Out
mkdir git-test
git init
git status

# Checking status
ls
touch test1.txt
ls
git status

# Adding
git add test1.txt
git status

# Commiting
git commit -m"archivo de prueba"
git status

# Adding all changes
ls
mkdir tests
touch test2.txt test3.txt tests/4.txt tests/5.txt
ls
git status

git add *.txt
git status

# Commiting al changes
git commit -m"mas archivos de prueba"
git status

# History
git log

# Remote repository
# crear repo en bitbucket
git remote add origin https://<USUARIO>@bitbucket.org/<USUARIO>/<REPO>.git
# git remote add origin ssh://git@bitbucket.org/<USUARIO>/<REPO>.git

# Pushing
git push -u origin master


# Pulling
## checkear bitbucket
## crear archivo desde bitbucket
git pull origin master

# Differences
# modificar archivo nuevo en editor de textos
git diff HEAD
git diff

# Staged Differences
git add tests/6.txt
git diff
git diff --staged

git reset tests/6.txt
git diff

# Undo
git checkout tests/6.txt
git diff

####################
# Clonar repo PDAN #
####################

# BRANCHES
# https://try.github.io/levels/1/challenges/18
git branch nuevo-feature
git checkout nuevo-feature

# realizar cambios
git status
git add .
git commit -am'nuevos cambios'

git push origin nuevo-feature
# chequear bitbucket

# merge
git status
git checkout master
git merge nuevo-feature
git status
git push
# chcequear bitbucket graph
# limpar git branch -d nuevo-feature
